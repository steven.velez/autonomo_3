# "Capitulo 5"
# Author: "Steven   Velez"
# email: "steven.velez@unl.edu.ec"

# Ejercicio 1: Escribe un programa que lea repetidamente números hasta que el usuario introduzca “ﬁn”.
# Una vez se haya introducido “ﬁn”, muestra por pantalla el total, la cantidad de números y la media de
# esos números. Si el usuario introduce cualquier otra cosa que no sea un
# número, detecta su fallo usando try y except, muestra un mensaje de error y pasa al número siguiente

cont = 0
total = 0

while True:
    valor = input("Introduzca un número entero (o 'fin' para terminar): ")
    if valor.lower() in "fin":
        break
    try:
        total += int (valor)
        cont  += 1
        media = total/cont
    except :
        print("Valor erroneo. Intentalo de nuevo...")

print("El total es: ", total)
print("Introducido: ", cont, " numeros")
print("La media de los valores es: ", float(media), "=", int(media))
