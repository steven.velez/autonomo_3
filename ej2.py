# Author: "Steven   Velez"
# Email: "steven.velez@unl.edu.ec"

#Ejercicio 2: "Escribe otro programa que pida una lista de números como la anterior
# y al ﬁnal muestre por pantalla el máximo y mínimo de los números, en vez de la media

cont = 0
total = 0
lista = []

while True:

    valor = input("Introduce un número entero (o 'fin' para terminar): ")
    lista.append(valor)
    if valor.lower() in "fin":
        break
    try:
        total += float (valor)
        cont  += 1
        mayor = max (lista)
        menor = min (lista)
    except :
        print("ERROR, no es un numero\nIntentalo de nuevo...")

print("El total es: ", total)
print("Haz introducido: ", cont, " numeros")
print("Lista de numero:", lista)
print("El numero mayor es: ", float(mayor))
print("El numero menor es: ", float(menor))
